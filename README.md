# hw2
계산기 프로그램을 구현한다.
===========================

### 1. Calculator.java

<pre>
Only Button Register : 이벤트들을 xml 파일에서 선언한 각 버튼들의 id들을 알아내 배열에 저장한 후 반복문을 통해 이벤트 리스너 인터페이스에 등록한다.
번호 버튼은 button_number[], 연산 버튼은 button_operator[] 배열에 정수형(Integer) 타입으로 삽입한다. 

※ Calaulator 메소드 ※
연산자를 클릭했을 때 반응하는 이벤트를 처리하는 메소드는 Calculator 메소드이다.

※ 번호 버튼 id ※
buttonZero ~ button9 - 0 ~ 9 까지의 번호를 나타낸다.

※ 연산 버튼 id ※  
buttonAdd : 더하기(+)를 수행한다.
buttonSubtraction : 빼기(-)를 수행한다.
buttonMultiply : 곱하기(*)를 수행한다.
buttonDivide : 나누기(/)를 수행한다.
buttonEqual : 연산한 결과를 반환하는 역할(=)을 수행한다.
buttonClearText : 현재 수행 중인 연산 및 연산 결과를 모두 지우는 역할(c)을 수행한다.
	
※ 출력 레이블 id ※  		         
textView : 현 입력되는 연산을 출력하는 과정을 보여주는 창이다.
Result : 연산의 수행 과정과 연산의 결과 값을 반환해 보여주는 창이다.  (연산 기호는 출력되지 않는 버그 존재)
</pre>

###  2. Replace Calculator.java (+ Add, Minus, C )

<pre>
※ Num_Stack, History_Stack 메소드 ※
Num_Stack : 정수(Num)를 인자로 입력받는 Num_Stack - 더하기와 빼기의 기능 벡터 컬렉션(num_stack)을 이용해 구현한다. 
History_Stack : 문자열(S)을 인자로 입력받는 History Stack - 연산 과정을 출력하는 기능을 벡터 컬렉션(history_stack)을 이용해 구현한다.

※ 선언 변수 ※ 
Operland : buttonEqual 버튼을 눌렀을 경우 전에 해당 연산자를 눌렀을 때의 값을 저장하는 정수형 변수이다. (Ex. buttonAdd일 경우 Operland 값은 0)
Number : TextView로 들어온 입력 숫자를 정수형으로 변환한 값을 저장하는 정수형 변수이다.
Total_Sum : 더하거나 뺀 값들을 모두 더해 저장하는 정수형 변수이다. 
</pre>

###  3. Replace Calculator.java

<pre>
2번에서의 과정은 = 버튼을 클릭하는 순간 의외에도 +, -, *, / 버튼을 클릭했을 때도 연산을 수행하도록 했다.
하지만 이러한 방식을 사용할 경우 연산자의 우선순위를 처리하는데 문제가 생겨 +, -, *, / 버튼을 누를 때 연산의 과정들을 벡터 컬렉션(Input_Vector)에 담는다.
그리고 "=" 버튼을 눌렀을 때만 Input_Vector 벡터에 차례대로 쌓아놓은 숫자와 연산자들을 꺼내어 계산을 시행한다.

※ 선언 변수 ※ 
Number : TextView로 들어온 입력 숫자를 저장하는 문자형 변수
result : 결과를 출력할 실수형 변수
Input_Vector : 모든 숫자와 연산자들을 차례대로 담는 벡터
stack : Input_Vector 벡터에 있는 모든 값들을 저장하는 스택

※ 제거된 변수, 함수 ※  
Operland, Number, Total_Sum, Num_Stack, History_Stack

연산 원리는 스택에 들어온 숫자와 연산자들이 엇갈려서 쌓인다는 원리를 적용, 숫자는 인덱스의 값이 홀수, 연산자는 짝수인 층에 쌓인다. (인덱스는 1부터 시작)
1. 예를 들어 연산의 과정은 다음과 같다. [6 + 2 X 4 - 2]를 계산한다고 한다. stack 스택에도 이와 같이 처음부터 차례대로 저장된다. 
2. 연산 순위가 "+"와 "-"보다 높은 "X"가 포함된 2X3을 먼저 계산한 후 6을 stack에 다음과 같이 저장된다. [6 + 8 2 X 4 - 2]
3. 그리고 연산이 끝난 [2 X 4]를 스택에서 제거한다. 현재 stack 스택은 다음과 같다. [6 + 8 - 2]
4. 그리고 남은 연산 순위 최하위인 연산자 "+"와 "-"가 포함된 식을 계산해 지속적으로 result 변수에 집어넣고 Result TextView 창에 최종 연산 값을 출력한다.
</pre>

###  4. Replace Calculator.java

<pre>
3번에서 두 자릿수 이상의 연산을 수행하지 못한다는 문제가 발생 ---> 숫자 버튼 하나를 클릭할 때마다 바로 각기 다른 공간의 벡터에 쌓기 때문이다.
[Ex]. 15라는 숫자를 입력하면 1을 먼저 클릭하게 되는데 Input_Vector[0]에 1을, 그 후에 바로 5를 클릭하면 Input_Vector[1]에 5를, 각기 다른 공간에 집어넣게 되는 것이 문제

이를 해결하기 위해 스트링 버퍼를 이용 ---> 숫자 버튼을 누르면 스트링 버퍼(sb)에 append해서 들어오는 전에 클릭했던 숫자들과 합쳐서 스트링 버퍼에 넣는다.

※ 선언 변수, 함수 ※ 
sb : 들어오는 숫자를 집어넣을 스트링 버퍼 
fact : 팩토리얼 연산을 하는 함수

※ 추가된 번호 버튼 id ※
buttonPi : 파이(π) 연산을 나타낸다.
buttonExp : 지수(e) 연산을 나타낸다.

※ 추가된 연산 버튼 id ※  
buttonSquare : N 제곱(^) 연산을 수행한다.
buttonFact : 팩토리얼(!) 연산을 수행한다.
buttonRoot : 루트(√) 연산을 수행한다.
buttonMod : 나머지 연산(mod)을 수행한다.
buttonPercent : 백분율(%) 연산을 수행한다.
</pre>