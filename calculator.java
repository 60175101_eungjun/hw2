package com.example.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.*;
import java.util.*;

public class scientific_calculator extends Activity {
    Button button_operator [] = new Button[11];
    Integer Button_Operator [] = {
            R.id.buttonAdd, R.id.buttonSubtraction, R.id.buttonMultiply, R.id.buttonDivide,
            R.id.buttonEqual, R.id.buttonClearText, R.id.buttonSquare, R.id.buttonFact, R.id.buttonRoot,
            R.id.buttonMod, R.id.buttonPercent
    };

    Button button_number [] = new Button [12];
    Integer Button_Number [] = {
            R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5,
            R.id.button6, R.id.button7, R.id.button8, R.id.button9,R.id.buttonZero,
            R.id.buttonPi, R.id.buttonExp
    };

    String Num;
    double result = 0.0;
    Vector<String> Input_Vector = new Vector<>();
    StringBuffer sb = new StringBuffer();

    public static double fact(double n) {
        if (n <= 1) return 1;
        else return fact(n - 1) * n;
    }

    public void Calculate(){
        for(int i = 0; i < button_operator.length; i++){
            button_operator[i] = findViewById(Button_Operator[i]);
        }
        for(int i = 0; i < button_operator.length; i++){
            button_operator[i].setOnClickListener(new View.OnClickListener() {
                public void onClick (View v){
                    TextView textView = findViewById(R.id.textView1);
                    TextView Result = findViewById(R.id.History);
                    Num = textView.getText().toString();

                    if(Num.equals("e")){Input_Vector.add("2.718");}
                    else if(Num.equals("π")){Input_Vector.add("3.1415");}
                    else {Input_Vector.add(Num);}

                    switch (v.getId()) {
                        case R.id.buttonAdd:
                            Input_Vector.add("+");
                            textView.setText("");
                            Result.append("+");
                            sb.setLength(0);
                            break;

                        case R.id.buttonSubtraction:
                            Input_Vector.add("-");
                            textView.setText("");
                            Result.append("-");
                            sb.setLength(0);
                            break;

                        case R.id.buttonMultiply:
                            Input_Vector.add("X");
                            textView.setText("");
                            Result.append("X");
                            sb.setLength(0);
                            break;

                        case R.id.buttonDivide:
                            Input_Vector.add("/");
                            textView.setText("");
                            Result.append("/");
                            sb.setLength(0);
                            break;

                        case R.id.buttonSquare:
                            Input_Vector.add("^");
                            textView.setText("");
                            Result.append("^");
                            sb.setLength(0);
                            break;

                        case R.id.buttonExp:
                            Input_Vector.add("e");
                            textView.setText("");
                            Result.append("e");
                            sb.setLength(0);
                            break;

                        case R.id.buttonFact:
                            Input_Vector.add("!");
                            textView.setText("");
                            Result.append("!");
                            sb.setLength(0);
                            break;

                        case R.id.buttonRoot:
                            Input_Vector.add("√");
                            textView.setText("");
                            Result.append("√");
                            sb.setLength(0);
                            break;

                        case R.id.buttonMod:
                            Input_Vector.add("mod");
                            textView.setText("");
                            Result.append("mod");
                            sb.setLength(0);
                            break;

                        case R.id.buttonPercent:
                            Input_Vector.add("%");
                            textView.setText("");
                            Result.append("%");
                            sb.setLength(0);
                            break;

                        case R.id.buttonEqual:
                            for(int i = 1; i < Input_Vector.size(); i = i + 2) {
                                switch (Input_Vector.get(i)) {
                                    case "!":
                                        result = fact(Double.valueOf(Input_Vector.get(i - 1)));
                                        Input_Vector.add(i - 1, String.valueOf(result));
                                        Input_Vector.remove(i); Input_Vector.remove(i); Input_Vector.remove(i);
                                        i = i - 2;
                                        break;
                                }
                            }
                            for(int i = 1; i < Input_Vector.size(); i = i + 2) {
                                switch (Input_Vector.get(i)) {
                                    case "^":
                                        result = (Math.pow(Double.valueOf(Input_Vector.get(i - 1)), Double.valueOf(Input_Vector.get(i + 1))));
                                        Input_Vector.add(i - 1, String.valueOf(result));
                                        Input_Vector.remove(i); Input_Vector.remove(i); Input_Vector.remove(i);
                                        i = i - 2;
                                        break;
                                    case "√":
                                        result = (Math.sqrt(Double.valueOf(Input_Vector.get(i - 1))));
                                        Input_Vector.add(i - 1, String.valueOf(result));
                                        Input_Vector.remove(i); Input_Vector.remove(i); Input_Vector.remove(i);
                                        i = i - 2;
                                        break;
                                }
                            }
                            for(int i = 1; i < Input_Vector.size(); i = i + 2){
                                switch (Input_Vector.get(i)) {
                                    case "X" :
                                        result = Double.valueOf(Input_Vector.get(i - 1)) * Double.valueOf(Input_Vector.get(i + 1));
                                        Input_Vector.add(i - 1, String.valueOf(result));
                                        Input_Vector.remove(i);Input_Vector.remove(i);Input_Vector.remove(i);
                                        i = i - 2;
                                        break;
                                    case "/" :
                                        try {
                                            result = Double.valueOf(Input_Vector.get(i - 1)) / Double.valueOf(Input_Vector.get(i + 1));
                                            Input_Vector.add(i - 1, String.valueOf(result));
                                            Input_Vector.remove(i);Input_Vector.remove(i);Input_Vector.remove(i);
                                            i = i - 2;
                                        } catch (ArithmeticException e) {
                                            Toast.makeText(getApplicationContext(),"0으로 나누기 불가",Toast.LENGTH_LONG).show();
                                            textView.setText("");
                                            Result.setText("");
                                        }
                                        break;
                                    case "%" :
                                        result = Double.valueOf(Input_Vector.get(i - 1)) / (100);
                                        Input_Vector.add(i - 1, String.valueOf(result));
                                        Input_Vector.remove(i); Input_Vector.remove(i); Input_Vector.remove(i);
                                        i = i - 2;
                                        break;
                                    case "mod" :
                                        result = Double.valueOf(Input_Vector.get(i - 1)) % Double.valueOf(Input_Vector.get(i + 1));
                                        Input_Vector.add(i - 1, String.valueOf(result));
                                        Input_Vector.remove(i); Input_Vector.remove(i); Input_Vector.remove(i);
                                        i = i - 2;
                                        break;
                                }
                            }
                            for(int i = 1; i < Input_Vector.size(); i = i + 2){
                                if(i == 1){
                                    result = 0;
                                    result = result + Double.valueOf(Input_Vector.get(i - 1));
                                }switch(Input_Vector.get(i)){
                                    case "+" :
                                        result = result + Double.valueOf(Input_Vector.get(i + 1));
                                        break;
                                    case "-" :
                                        result = result - Double.valueOf(Input_Vector.get(i + 1));
                                        break;
                                }
                            }
                            textView.setText("");
                            Result.setText(result + "");
                            break;

                        case R.id.buttonClearText:
                            textView.setText("");
                            Result.setText("");
                            Input_Vector.clear();
                            sb.setLength(0);
                            result = 0;
                            Num = "";
                            break;
                    }
                }
            });
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scientific_calculator_ui);
        for(int i = 0;i < button_number.length; i++){
            button_number[i] = findViewById(Button_Number[i]);
        }
        for(int i = 0; i < button_number.length; i++){
            button_number[i].setOnClickListener(new View.OnClickListener() {
                public void onClick (View v){
                    TextView textView = findViewById(R.id.textView1);
                    TextView Result = findViewById(R.id.History);
                    switch (v.getId()) {
                        case R.id.button1 : textView.append("1"); Result.append("1"); sb.append("1"); Calculate(); break;
                        case R.id.button2 : textView.append("2"); Result.append("2"); sb.append("2"); Calculate(); break;
                        case R.id.button3 : textView.append("3"); Result.append("3"); sb.append("3"); Calculate(); break;
                        case R.id.button4 : textView.append("4"); Result.append("4"); sb.append("4"); Calculate(); break;
                        case R.id.button5 : textView.append("5"); Result.append("5"); sb.append("5"); Calculate(); break;
                        case R.id.button6 : textView.append("6"); Result.append("6"); sb.append("6"); Calculate(); break;
                        case R.id.button7 : textView.append("7"); Result.append("7"); sb.append("7"); Calculate(); break;
                        case R.id.button8 : textView.append("8"); Result.append("8"); sb.append("8"); Calculate(); break;
                        case R.id.button9 : textView.append("9"); Result.append("9"); sb.append("9"); Calculate(); break;
                        case R.id.buttonZero : textView.append("0"); Result.append("0"); sb.append("0"); Calculate(); break;
                        case R.id.buttonPi : textView.append("π"); Result.append("π"); sb.append("3.1415"); Calculate(); break;
                        case R.id.buttonExp : textView.append("e"); Result.append("e"); sb.append("2.718"); Calculate(); break;
                    }
                }
            });
        }
    }
}